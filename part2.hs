{- |
Module      :  Part 2 of the discrete mathematics homework 
Description :  Implementation of the MCP heuristic described in the homework answer sheet
Copyright   :  <Helio Wang>
License     :

Maintainer  :  <helioaimelesoleil@gmail.com>
Stability   :  stable
Portability :  portable

-}

import Data.List 
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

main = do
    inputFile <- readFile "input.txt"
    let inputLines = lines $ inputFile
        species = processFirstLine $ head $ inputLines
        incompatList = processList $ drop 2 $ inputLines
    writeFile "output.txt" $ formatOutput $ mcp species incompatList
    
type Bacteria = Char
type IncompatList = Map.Map Bacteria (Set.Set Bacteria)
type Petri = Set.Set Bacteria

-- implementation of the heuristic for the MCP problem
mcp :: [Bacteria] -> IncompatList -> [Petri]
mcp xs lst = foldl (mcp' lst) [] xs

mcp' :: IncompatList -> [Petri] -> Bacteria -> [Petri]
mcp' lst ps x = case filter (isCompatible lst x) ps  of
                     [] -> (Set.singleton x) : ps
                     cps -> let p = head cps in (Set.insert x p) : delete p ps
            
-- check the compatibility between a species and a Petri dish
isCompatible :: IncompatList -> Bacteria -> Petri -> Bool
isCompatible lst x p = all id $ Set.map (isCompatible' lst x) p

-- check the compatibility between two species
isCompatible' :: IncompatList -> Bacteria -> Bacteria  -> Bool
isCompatible' lst x y = case Map.lookup x lst of
                             Just adj -> Set.notMember y adj
                             Nothing -> True


-- ad hoc function formatting the output
formatOutput :: [Petri] -> String
formatOutput ps = "We use " ++ (show k) ++ " petri " ++ dish
    ++ (init $ unlines $ map ((++ ".") . f) ps)
    where k = length ps
          f = intersperse ',' . drop 10 . init . show
          dish = if (k==1) then "dish.\n" else "dishes.\n"

-- ad hoc function processing the first line of the input file
processFirstLine :: String -> [Bacteria]
processFirstLine = map head . wordsWhen (==',') . drop 2 . dropWhile (/=':') . init

-- ad hoc function processing the lines 3 to end of the input file
-- simple conversion from edge list to adjacency list
processList :: [String] -> IncompatList
processList [] = Map.empty
processList (x:xs) = Map.insertWithKey f a (Set.singleton b) $
                     Map.insertWithKey f b (Set.singleton a) map
    where map = processList xs
          (a:ys, _:b:zs) = break (==',') x
          f = \key new_value old_value -> Set.union new_value old_value
          
-- string split utility, avoid import
wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s =  case dropWhile p s of
                      "" -> []
                      s' -> w : wordsWhen p s''
                            where (w, s'') = break p s'

                            