"""
Part 2 of the discrete mathematics homework
Implementation of the MCP heuristic described in the homework answer sheet
"""

__author__ = 'Helio'

def edge_list_to_adj_list(nodes, edge_list):
    """simple conversion from edge list to adjacency list"""
    adj_list = {}
    for node in nodes:
        adj_list[node] = set()
    for u, v in edge_list:
        adj_list[u].add(v)
        adj_list[v].add(u)
    return adj_list


def is_compatible(incompat_list, species, petri):
    """check the compatibility between a species and a Petri dish"""
    for another_species in petri:
        if another_species in incompat_list[species]:
            return False
    return True


def mcp(species, incompat_list):
    """returns a compatible partition"""
    petri = [{species[0]}]
    num_petri = 1
    for i in xrange(1, len(species)):
        current_species = species[i]
        added = False
        for current_petri in petri:
            if is_compatible(incompat_list, current_species, current_petri):
                current_petri.add(current_species)
                added = True
                break
        if not added:
            num_petri += 1
            petri.append({current_species})
    return petri


if __name__ == '__main__':
    # read the input
    with open('input.txt') as f:
        species = f.readline().rstrip().split(':')[1][1:-1].split(',')
        f.readline()
        incompat_list = edge_list_to_adj_list(species,
                                              map(lambda line: line.rstrip().split(','), f.readlines()))
        
    # run the MCP heuristic
    partition = mcp(species, incompat_list)
    
    # output 
    output = '\n'.join(['We use %d petri dish%s.' % (len(partition), {True:'', False: 'es'}[len(partition) == 1])]
                        + map(lambda p: ','.join(map(str, sorted(list(p)))) + '.', partition))
    f = open('output.txt', 'w')
    f.write(output)
    f.close()
