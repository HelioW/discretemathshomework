# README #


## Description ##

The source code for the discrete mathematics homework, part 2.


## Instructions ##

### Dependencies ###
None.

### General remark ###
the input file is assumed to be `input.txt`.

* How to run `part2.py`:
```
python2 part2.py

```

There is no Python3 compatibilty.

* How to run `part2.hs`:
```

runhaskell part2.hs

```

Note that the `insert` operation on `Data.Set.Set` is O(logN), not O(1).

## Contact ##
<helioaimelesoleil@gmail.com> 
